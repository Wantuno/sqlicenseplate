test("isValidPlate", function (assert) {
	
	//Tested ALL cases where the plate is OK but only a handfull of the cases where it is NOK of those pointed int he explanation, 
	//since there are over a hundred.
	
	//Valid plate tests
    assert.equal(isValidPlate('1234DBC'), true, "OK");
    assert.equal(isValidPlate('1134DBC'), true, "OK");
    assert.equal(isValidPlate('1114DBC'), true, "OK");
    assert.equal(isValidPlate('1111DBC'), true, "OK");
    assert.equal(isValidPlate('1234DDC'), true, "OK");
    assert.equal(isValidPlate('1134DDC'), true, "OK");
    assert.equal(isValidPlate('1114DDC'), true, "OK");
    assert.equal(isValidPlate('1111DDC'), true, "OK");
    assert.equal(isValidPlate('1234DDD'), true, "OK");
    assert.equal(isValidPlate('1134DDD'), true, "OK");
    assert.equal(isValidPlate('1114DDD'), true, "OK");
    assert.equal(isValidPlate('1111DDD'), true, "OK");
    
    //Invalid plate tests
    assert.equal(isValidPlate('X23XDBC'), false, "NOK");
    assert.equal(isValidPlate('1XX4DB9'), false, "NOK");
    assert.equal(isValidPlate('1X34D9C'), false, "NOK");
    assert.equal(isValidPlate('XXX4D9C'), false, "NOK");
    assert.equal(isValidPlate('1XXX9BC'), false, "NOK");
    assert.equal(isValidPlate('XXX4D99'), false, "NOK");
    assert.equal(isValidPlate('123X999'), false, "NOK");
    assert.equal(isValidPlate('XX3X999'), false, "NOK");  
    
});
